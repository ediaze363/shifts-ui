const headingMain = {
    text: 'Navigation',
    heading: true
};

const homeMenu = {
    text: 'Dashboard',
    link: '/dashboard',
    icon: 'icon-grid'
};

const shiftsMenu = {
    text: 'Shifts',
    link: '/shifts',
    icon: 'icon-clock',
    submenu: [
        {
            text: 'List',
            link: '/shifts/user-list'
        },
        {
            text: 'Report',
            link: '/shifts/v2'
        }
    ]
};

const adminMenu = {
    text: 'Admin',
    link: '/admin',
    icon: 'icon-settings',
    submenu: [
        {
            text: 'Users',
            link: '/admin/users/user-list'
        },
        {
            text: 'Global Settings',
            link: '/admin/preferences/preferences-list'
        }
    ]
};

export const menu = [
    headingMain,
    homeMenu,
    shiftsMenu,
    adminMenu
];
